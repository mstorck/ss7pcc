<?php
/*
    This file is part of SS7 Point Code Converter (SS7PCC).

    Copyright 2006, 2007, 2008, 2009, 2010, 2011, 2012
    Marc Storck <marc@storck.lu>

    SS7PCC is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SS7PCC is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with SS7PCC.  If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * ss7pcc.class.php, SS7 Point Code Converter PHP Class
 *
 * This file contains the main class which provides all necessary functions to
 * convert SS7 signalling point codes any supported format to binary and vice-
 * versa.
 *
 * @author Marc Storck <marc@storck.lu>
 * @copyright Copyright 2006, 2007, 2008 ,2009, 2010, 2011, 2012 Marc Storck <marc@storck.lu>
 * @license http://www.gnu.org/licenses/gpl.txt GNU General Public License
 */

/**
 * SS7PCC class provides all necessary function to de- and encode SPC. There is
 * no constructor for the class yet, so you need to call the functions
 * individually.
 *
 * @author Marc Storck <marc@storck.lu>
 * @copyright Copyright 2006, 2007, 2008 ,2009, 2010, 2011, 2012 Marc Storck <marc@storck.lu>
 * @license http://www.gnu.org/licenses/gpl.txt GNU General Public License
 */
class SS7PCC
{
  /**
   * Binary to Decimal encoder function
   *
   * This function encodes a binary SPC to the decimal counterpart.
   *
   * @author Marc Storck <marc@storck.lu>
   * @copyright Copyright 2006, 2007, 2008 ,2009, 2010, 2011, 2012 Marc Storck <marc@storck.lu>
   * @license http://www.gnu.org/licenses/gpl.txt GNU General Public License
   * @param string $bin the SPC in binary format
   * @return integer the SPC in decimal format
   */
  function func_bin2dec($bin)
  {
    $return = bindec($bin);
    return($return);
  }

  /**
   * Binary to Hexadecimal encoder function
   *
   * This function encodes a binary SPC to the hexadecimal counterpart.
   *
   * @author Marc Storck <marc@storck.lu>
   * @copyright Copyright 2006, 2007, 2008 ,2009, 2010, 2011, 2012 Marc Storck <marc@storck.lu>
   * @license http://www.gnu.org/licenses/gpl.txt GNU General Public License
   * @param string $bin the SPC in binary format
   * @return string the SPC in hexadecimal format
   */
  function func_bin2hex($bin)
  {
    $return = "0x".dechex($this->func_bin2dec($bin));
    return($return);
  }

  /**
   * Binary to ITU format encoder function
   *
   * This function encodes a binary SPC to the ITU format. The International
   * Telecommunication Union splits the SPC number into 3 blocks of 3,8 and 3
   * bits, each separated with a dash, e.g. 3-8-3
   *
   * @author Marc Storck <marc@storck.lu>
   * @copyright Copyright 2006, 2007, 2008 ,2009, 2010, 2011, 2012 Marc Storck <marc@storck.lu>
   * @license http://www.gnu.org/licenses/gpl.txt GNU General Public License
   * @param string $bin the SPC in binary format
   * @return string the SPC in ITU format
   */
  function func_bin2itu($bin)
  {
    $p1 = $this->func_bin2dec(substr($bin,0,3));
    $p2 = $this->func_bin2dec(substr($bin,strlen($bin)-11,8));
    while(strlen($p2)<3)
    {
      $p2 = "0".$p2;
    }
    $p3 = $this->func_bin2dec(substr($bin,strlen($bin)-3,3));
    $return = $p1."-".$p2."-".$p3;
    return($return);
  }

  /**
   * Binary to Luxembourg national format encoder function
   *
   * This function encodes a binary SPC to Luxembourg national format
   * counterpart. Luxembourg's Regulator (ILR) has defined the national format
   * similar to the ITU format but uses blocks of 7,4 and 3 bits, e.g. 7-4-3
   *
   * @author Marc Storck <marc@storck.lu>
   * @copyright Copyright 2006, 2007, 2008 ,2009, 2010, 2011, 2012 Marc Storck <marc@storck.lu>
   * @license http://www.gnu.org/licenses/gpl.txt GNU General Public License
   * @param string $bin the SPC in binary format
   * @return string the SPC in Luxembourg national format
   */
  function func_bin2ilr($bin)
  {
    $p1 = $this->func_bin2dec(substr($bin,0,7));
    while(strlen($p1)<3)
    {
      $p1 = "0".$p1;
    }
    $p2 = $this->func_bin2dec(substr($bin,strlen($bin)-7,4));
    while(strlen($p2)<2)
    {
      $p2 = "0".$p2;
    }
    $p3 = $this->func_bin2dec(substr($bin,strlen($bin)-3,3));
    $return = $p1."-".$p2."-".$p3;
    return($return);
  }

  /**
   * Binary to German national format encoder function
   *
   * This function encodes a binary SPC to the German national format
   * counterpart. The German regulator (BundesNetzAgentur) has defined the
   * national format similar to the ITU  format but uses blocks of 4,3,4 and 3
   * bits, e.g. 4-3-4-3
   *
   * @author Marc Storck <marc@storck.lu>
   * @copyright Copyright 2006, 2007, 2008 ,2009, 2010, 2011, 2012 Marc Storck <marc@storck.lu>
   * @license http://www.gnu.org/licenses/gpl.txt GNU General Public License
   * @param string $bin the SPC in binary format
   * @return string the SPC in German national format
   */
    function func_bin2bna($bin)
  {
    $p1 = $this->func_bin2dec(substr($bin,0,4));
    while(strlen($p1)<2)
    {
      $p1 = "0".$p1;
    }
    $p2 = $this->func_bin2dec(substr($bin,4,3));
    $p3 = $this->func_bin2dec(substr($bin,7,4));
    while(strlen($p3)<2)
    {
      $p3 = "0".$p3;
    }
    $p4 = $this->func_bin2dec(substr($bin,11,3));
    $return = $p1."-".$p2."-".$p3."-".$p4;
    return($return);
  }

  /**
   * Decimal to Binary decoder function
   *
   * This function decodes a decimal SPC to the binary counterpart.
   *
   * @author Marc Storck <marc@storck.lu>
   * @copyright Copyright 2006, 2007, 2008 ,2009, 2010, 2011, 2012 Marc Storck <marc@storck.lu>
   * @license http://www.gnu.org/licenses/gpl.txt GNU General Public License
   * @param integer $dec the SPC in decimal format
   * @return string the SPC in binary format
   */
  function func_dec2bin($dec)
  {
    $bin = decbin($dec);
    while(strlen($bin)<14)
    {
      $bin = "0".$bin;
    }
    $return = $bin;
    return($return);
  }

  /**
   * Hexadecimal to Binary decoder function
   *
   * This function decodes a hexadecimal SPC to the binary counterpart.
   *
   * @author Marc Storck <marc@storck.lu>
   * @copyright Copyright 2006, 2007, 2008 ,2009, 2010, 2011, 2012 Marc Storck <marc@storck.lu>
   * @license http://www.gnu.org/licenses/gpl.txt GNU General Public License
   * @param integer $hex the SPC in hexadecimal format
   * @return string the SPC in binary format
   */
    function func_hex2bin($hex)
  {
    $dec = hexdec($hex);
    $bin = $this->func_dec2bin($dec);
    $return = $bin;
    return($return);
  }

  /**
   * ITU format to Binary decoder function
   *
   * This function decodes an SPC in ITU format to the binary counterpart.
   *
   * @author Marc Storck <marc@storck.lu>
   * @copyright Copyright 2006, 2007, 2008 ,2009, 2010, 2011, 2012 Marc Storck <marc@storck.lu>
   * @license http://www.gnu.org/licenses/gpl.txt GNU General Public License
   * @param string $itu the SPC in ITU format
   * @return string the SPC in binary format
   */
  function func_itu2bin($itu)
  {
    $c = explode("-", $itu);
    $p1 = decbin($c[0]);
    while(strlen($p1)<3)
    {
      $p1 = "0".$p1;
    }
    $p2 = decbin($c[1]);
    while(strlen($p2)<8)
    {
      $p2 = "0".$p2;
    }
    $p3 = decbin($c[2]);
    while(strlen($p3)<3)
    {
      $p3 = "0".$p3;
    }
    $return = $p1.$p2.$p3;
    return($return);
  }

  /**
   * ILR format to Binary decoder function
   *
   * This function decodes an SPC in Luxembourg national format to the binary
   * counterpart.
   *
   * @author Marc Storck <marc@storck.lu>
   * @copyright Copyright 2006, 2007, 2008 ,2009, 2010, 2011, 2012 Marc Storck <marc@storck.lu>
   * @license http://www.gnu.org/licenses/gpl.txt GNU General Public License
   * @param string $ilr the SPC in ILR format
   * @return string the SPC in binary format
   */
  function func_ilr2bin($ilr)
  {
    $c = explode("-", $ilr);
    $p1 = decbin($c[0]);
    while(strlen($p1)<7)
    {
      $p1 = "0".$p1;
    }
    $p2 = decbin($c[1]);
    while(strlen($p2)<4)
    {
      $p2 = "0".$p2;
    }
    $p3 = decbin($c[2]);
    while(strlen($p3)<3)
    {
      $p3 = "0".$p3;
    }
    $return = $p1.$p2.$p3;
    return($return);
  }

  /**
   * BNA format to Binary decoder function
   *
   * This function decodes an SPC in German national format to the binary
   * counterpart.
   *
   * @author Marc Storck <marc@storck.lu>
   * @copyright Copyright 2006, 2007, 2008 ,2009, 2010, 2011, 2012 Marc Storck <marc@storck.lu>
   * @license http://www.gnu.org/licenses/gpl.txt GNU General Public License
   * @param string $bna the SPC in BNA format
   * @return string the SPC in binary format
   */
  function func_bna2bin($bna)
  {
    $c = explode("-", $bna);
    $p1 = decbin($c[0]);
    while(strlen($p1)<4)
    {
      $p1 = "0".$p1;
    }
    $p2 = decbin($c[1]);
    while(strlen($p2)<3)
    {
      $p2 = "0".$p2;
    }
    $p3 = decbin($c[2]);
    while(strlen($p3)<4)
    {
      $p3 = "0".$p3;
    }
    $p4 = decbin($c[3]);
    while(strlen($p4)<3)
    {
      $p4 = "0".$p4;
    }
    $return = $p1.$p2.$p3.$p4;
    return($return);
  }
}
?>
