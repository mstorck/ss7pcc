<html>
<head>
<title>SS7 Point Code Converter</title>
</head>
<body>
<pre>
	This file is part of SS7 Point Code Converter (SS7PCC)

	Copyright 2006, 2007, 2008, 2009, 2010, 2011, 2012 Marc Storck <marc@storck.lu>

    SS7PCC is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SS7PCC is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with SS7PCC.  If not, see <http://www.gnu.org/licenses/>.
</pre>
<form action="example.php" method="POST">
<SELECT NAME="format">
<OPTION VALUE="dec">decimal
<OPTION VALUE="bin">binary
<OPTION VALUE="hex">hexadecimal
<OPTION VALUE="itu">ITU
<OPTION VALUE="ilr">ILR
<OPTION VALUE="bna">BNA
</SELECT>
<br>
<INPUT TYPE=text NAME=code SIZE=14>
<br>
<input type=hidden name=submit value=1>
<INPUT TYPE=SUBMIT VALUE="submit">
<br>

<?php
if(!$_REQUEST["submit"]) die;

require_once("ss7pcc.class.php");

$ss7 = new SS7PCC();

switch($_REQUEST["format"])
{
  case "dec":
    $bin = $ss7->func_dec2bin($_REQUEST["code"]);
    break;

  case "bin":
    $bin = $_REQUEST["code"];
    while(strlen($bin)<14)
    {
      $bin = "0".$bin;
    }
    break;

  case "hex":
    $bin = $ss7->func_hex2bin(str_replace("0x", "" ,$_REQUEST["code"]));
    break;

  case "itu":
    $bin = $ss7->func_itu2bin($_REQUEST["code"]);
    break;

  case "ilr":
    $bin = $ss7->func_ilr2bin($_REQUEST["code"]);
    break;

  case "bna":
    $bin = $ss7->func_bna2bin($_REQUEST["code"]);
    break;
}
echo "Binary: ".$bin."<br>";
echo "Decimal: ".$ss7->func_bin2dec($bin)."<br>";
echo "Hexdecimal: ".$ss7->func_bin2hex($bin)."<br>";
echo "ITU: ".$ss7->func_bin2itu($bin)."<br>";
echo "ILR: ".$ss7->func_bin2ilr($bin)."<br>";
echo "BNA: ".$ss7->func_bin2bna($bin)."<br>";
?>

</body>
</html>
